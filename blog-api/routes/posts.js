let store = require('../store.js')
let app = require('../server.js')

function postIsValid(post) {
  return post.name.trim() && post.url.trim() && post.text.trim()
}

function postValidation(req, res, next) {
  if (!postIsValid(req.body)) return res.sendStatus(401)
  next()
}

module.exports = {
  getPosts(req, res) {
    res.status(200).send(store.posts)
  },
  addPost(req, res) {
    newPostId = store.posts.length
    store.posts.push(req.body)
    res.status(201).send({postId: newPostId})
  },
  updatePost(req, res) {
    postId = req.params.id
    store.posts[postId] = Object.assign(store.posts[postId], req.body)
    res.status(204).send(store.posts[postId])
  },
  removePost(req, res) {
    postId = req.params.id
    store.posts.splice(postId, 1)
    res.sendStatus(204)
  },
  postValidation
}
