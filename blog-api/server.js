const express = require('express')
const logger = require('morgan')
const errorhandler = require('errorhandler')
const bodyParser = require('body-parser')
const routes = require('./routes')
const posts = routes.posts
const comments = routes.comments

let app = module.exports = express()
app.use(bodyParser.json())
app.use(logger('dev'))
app.use(errorhandler())

app.route('/posts')
  .get(posts.getPosts)
  .post(posts.postValidation, posts.addPost)

app.route('/posts/:postId/')
  .put(posts.postValidation, posts.updatePost)
  .delete(posts.removePost)

app.route('/posts/:postId/comments')
  .get(comments.getComments)
  .post(comments.commentValidation, comments.addComment)

app.route('/posts/:postId/comments/:commentId')
  .put(comments.commentValidation, comments.updateComment)
  .delete(comments.removeComment)

app.listen(3000)
