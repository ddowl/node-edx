let store = require('../store.js')
let app = require('../server.js')

function commentIsValid(comment) {
  console.log(comment)
  return comment.text && comment.text.trim()
}

function commentValidation(req, res, next) {
  if (!commentIsValid(req.body)) return res.sendStatus(401)
  next()
}

function commentAccess(req) {
  postId = req.params.postId
  commentId = req.params.commentId
  comments = store.posts[postId].comments
  return {comments: comments, commentId: commentId}
}

module.exports = {
  getComments(req, res) {
    postId = req.params.postId
    res.status(200).send(store.posts[postId].comments)
  }, 
  addComment(req, res) {
    c = commentAccess(req)
    newCommentId = c.comments.length
    c.comments.push(req.body.text)
    res.status(201).send({commentId: newCommentId})
  },
  updateComment(req, res) {
    c = commentAccess(req)
    c.comments[c.commentId] = Object.assign(c.comments[c.commentId], req.body.text)
    res.status(200)
  },
  removeComment(req, res) {
    c.comments.splice(c.commentId, 1)
    res.status(204)
  },
  commentValidation,
}
